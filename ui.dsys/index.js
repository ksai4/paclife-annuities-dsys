const path = require('path');
const fs = require('fs');
const version = 'version-0.0.5';
let { globalSetting, customFileFormat } = require('./helper');
const sourceFile = path.join(__dirname, "node_modules", "@paclife", "plds.tokens", "figmagic");

fs.readdir(sourceFile, function (err, files) {
  if (err) {
    return console.log('Unable to scan directory: ' + err);
  }
  files.forEach(function (file) {
    if (file === 'index.js') return
    if (file.includes('.js')) {
      const module = require(`${sourceFile}/${file}`);
      let lessFileData = '';
      // split file name .js
      let splitFileName = file.split('.js');
      const overRideFileData = globalSetting(splitFileName);
      // merged design token object and custom object
      const tokenObject = { ...module, ...overRideFileData };

      Object.keys(tokenObject).forEach((el) => {
        lessFileData = customFileFormat(file, lessFileData, tokenObject, el);
      })
      file = file.replace('.js', '');
      const destinationPath = path.join(__dirname, "..", "ui.apps", "src", "main", "content", "jcr_root", "apps", "paclife-annuities-dsys", "clientlibs", "clientlib-site", "css", "design-tokens", version);
      fs.writeFile(`${destinationPath}/${file}.less`, lessFileData, (err) => {
        if (err)
          console.log(err);
        else {
          console.log("File written successfully\n");
          console.log("The written has the following contents:");
          console.log(fs.readFileSync(`${destinationPath}/${file}.less`, "utf8"));
        }
      });
    }
  });
});