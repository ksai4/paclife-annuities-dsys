const overrideFile = require('../ui.apps/src/main/content/jcr_root/apps/paclife-annuities-dsys/clientlibs/clientlib-site/css/override-design-tokens/constant')
const globalSetting = (splitFileName) => {
  // filter object
  const themeObject = Object.keys(overrideFile)
    .filter(key => splitFileName[0].includes(key))
    .reduce((obj, key) => {
      obj[key] = overrideFile[key];
      return obj;
    }, {});

  let themeFileData = '';
  // assign value in object
  Object.values(themeObject).forEach((data) => {
    themeFileData = data;
  });
  return themeFileData;
}

// create custom format when design token file generate
const customFileFormat = (file, lessFileData, tokenObject, el) => {
  switch (file) {
    case "fontWeights.js":
      return lessFileData + "\n" + "@fw" + el + " : " + tokenObject[el] + ";"
      break;
    case "durations.js":
      return lessFileData + "\n" + "@" + el + " : " + tokenObject[el] + "s" + ";"
      break;
    case "fontFamilies.js":
      return lessFileData + "\n" + "@" + el + " : " + "'" + tokenObject[el] + "'" + ";"
      break;
    default:
      return lessFileData + "\n" + "@" + el + " : " + tokenObject[el] + ";"
  }
}
module.exports = {
  globalSetting,
  customFileFormat
};
